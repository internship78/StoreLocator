<?php

declare(strict_types=1);

namespace Elogic\Internship\Controller\Adminhtml\StoreLocator;

use Elogic\Internship\Api\Data\StoreLocatorInterface;
use Elogic\Internship\Api\Data\StoreLocatorInterfaceFactory;
use Elogic\Internship\Api\StoreLocatorRepositoryInterface;

use Elogic\Internship\Model\GeoCode;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Catalog\Model\ImageUploader;
use Magento\Framework\Serialize\Serializer\Json;

class Save extends Action
{
    /**
     * @var RedirectFactory
     */
    private $redirectFactory;

    /**
     * @var $storeLocatorFactory
     */
    private $storeLocatorFactory;
    /**
     * @var StoreLocatorRepositoryInterface
     */
    private $storeRepository;
    /**
     * @var ImageUploader
     */
    private $imageUploader;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var GeoCode
     */
    private GeoCode $geoCode;

    /**
     * @param Context $context
     * @param RedirectFactory $redirectFactory
     * @param StoreLocatorInterfaceFactory $storeLocatorFactory
     * @param StoreLocatorRepositoryInterface $storeRepository
     * @param ImageUploader $imageUploader
     * @param GeoCode $geoCode
     * @param Json $json
     */
    public function __construct(
        Context $context,
        RedirectFactory $redirectFactory,
        StoreLocatorInterfaceFactory $storeLocatorFactory,
        StoreLocatorRepositoryInterface $storeRepository,
        ImageUploader $imageUploader,
        GeoCode $geoCode,
        Json $json
    ) {
            parent::__construct($context);
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->storeRepository = $storeRepository;
        $this->imageUploader = $imageUploader;
        $this->redirectFactory = $redirectFactory;
        $this->geoCode = $geoCode;
        $this->json = $json;
    }

    /**
     * @throws \Exception
     */
    public function execute()
    {
        $redirectResult = $this->redirectFactory->create();
        $store = $this->storeLocatorFactory->create();
        $data = $this->getRequest()->getPostValue();

        if (!$data['store_entity_id']) {
            $data['store_entity_id'] = null;
        } else {
            $store->setId($data['store_entity_id']);
        }
        $store->setName($data['name']);
        $store->setDescription($data['description']);
        $store->setAddress($data['address']);
        $store = $store->setLatitude($data['latitude']);
        $store = $store->setLongitude($data['longitude']);
        $store = $this->setImage($data, $store);
        $store = $this->setSchedule($data, $store);
        $this->storeRepository->save($store);

        $redirectResult->setPath('*/*/index');
        return $redirectResult;
    }

    /**
     * @param $data
     * @param $store
     * @return StoreLocatorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function setImage($data, $store): StoreLocatorInterface
    {
        if (isset($data['image'][0]['name']) && isset($data['image'][0]['tmp_name'])) {
            $data['image'] = $data['image'][0]['name'];
            $this->imageUploader->moveFileFromTmp($data['image']);
        } elseif (isset($data['image'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
            $data['image'] = $data['image'][0]['name'];
        } else {
            $data['image'] = '';
        }
        $store->setImage($data['image']);
        return $store;
    }

    /**
     * @param $data
     * @param $store
     * @return StoreLocatorInterface
     */
    public function setSchedule($data, $store): StoreLocatorInterface
    {
        if (isset($data['schedule'])) {
            $store->setSchedule($this->json->serialize($data['schedule']));
        }
        return $store;
    }
}
