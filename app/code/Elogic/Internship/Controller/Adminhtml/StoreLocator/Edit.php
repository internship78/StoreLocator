<?php

declare(strict_types=1);

namespace Elogic\Internship\Controller\Adminhtml\StoreLocator;

use Elogic\Internship\Api\StoreLocatorRepositoryInterface;
use Elogic\Internship\Api\Data\StoreLocatorInterfaceFactory;
use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;

class Edit extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var StoreLocatorInterfaceFactory
     */
    protected $storeFactory;
    /**
     * @var StoreLocatorRepositoryInterface
     */
    protected $storeRepository;
    /**
     * @var
     */
    protected $dataProvider;
    /**
     * @var
     */
    protected $coreRegistry;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param Action\Context $context
     * @param StoreLocatorInterfaceFactory $storeInterfaceFactory
     * @param StoreLocatorRepositoryInterface $storeRepository
     * @param StoreManagerInterface $storeManager
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        StoreLocatorInterfaceFactory $storeInterfaceFactory,
        StoreLocatorRepositoryInterface $storeRepository,
        StoreManagerInterface $storeManager,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->storeRepository = $storeRepository;
        $this->storeFactory = $storeInterfaceFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {

        $storeId = (int) $this->getRequest()->getParam('store_entity_id');
        if ($storeId) {
            $store = $this->storeRepository->getById($storeId);
            if (!$store->getId()) {
                $this->messageManager->addErrorMessage(__('No store with that id'));
                return $this->resultRedirectFactory->create()->setPath('*/*/');
            }
        }

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Elogic_Internship::elogicinternship');
        $resultPage->getConfig()->getTitle()->prepend(__('Store '));
        $resultPage->addHandle('elogic_store_entity' . $storeId);
        return $resultPage;
    }
}
