<?php

declare(strict_types=1);

namespace Elogic\Internship\Controller\adminhtml\StoreLocator;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

class Index extends \Magento\Backend\App\Action implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    public const ADMIN_RESOURCE = 'Elogic_Internship::storelocator';

    /**
     * Description
     * @return ResultInterface
     */

    public function execute(): ResultInterface
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend((__('Store Locator')));

        return $resultPage;
    }
}
