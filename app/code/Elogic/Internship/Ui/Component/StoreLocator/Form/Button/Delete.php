<?php

declare(strict_types=1);

namespace Elogic\Internship\Ui\Component\StoreLocator\Form\Button;

use Elogic\Internship\Model\Authorization;

class Delete implements \Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface
{
    /**
     * @var \Magento\Backend\Model\UrlInterface $urlBuilder
     */
    private \Magento\Backend\Model\UrlInterface $urlBuilder;

    /**
     * @var \Magento\Framework\App\RequestInterface $request
     */
    private \Magento\Framework\App\RequestInterface $request;

    /**
     * @var \Magento\Framework\Escaper $escaper
     */
    private \Magento\Framework\Escaper $escaper;

    /**
     * Delete constructor.
     * @param \Magento\Backend\Model\UrlInterface $urlBuilder
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\Escaper $escaper
     */
    public function __construct(
        \Magento\Backend\Model\UrlInterface $urlBuilder,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Escaper $escaper
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->request = $request;
        $this->escaper = $escaper;
    }

    /**
     * Get button configuration
     *
     * @return array
     */
    public function getButtonData(): array
    {
        $url = $this->urlBuilder->getUrl('*/*/delete');
        $message = $this->escaper->escapeJs(
            $this->escaper->escapeHtml(__('Are you sure you want to delete this store?'))
        );
        $storeId = (int) $this->request->getParam('store_entity_id');
        $buttonConfiguration = [];

        if ($storeId) {
            $buttonConfiguration = [
                'label' => __('Delete'),
                'class' => 'delete primary',
                'on_click' => "deleteConfirm('$message', '$url', {data:{store_entity_id:$storeId}})",
                'data_attribute' => [
                    'mage_init' => [
                        'button' => [
                            'event' => 'delete'
                        ],
                    ],
                    'form-role' => 'delete'
                ],
                'aclResource' => Authorization::ACTION_STORE_LOCATOR_DELETE,
                'sort_order' => 20
            ];
        }
        return $buttonConfiguration;
    }
}
