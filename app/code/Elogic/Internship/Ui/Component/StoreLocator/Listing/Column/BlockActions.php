<?php

declare(strict_types=1);

namespace Elogic\Internship\Ui\Component\StoreLocator\Listing\Column;

use Elogic\Internship\Model\Authorization;

class BlockActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    private const URL_PATH_EDIT = 'internship/storelocator/edit';

    /**
     * @var \Magento\Framework\UrlInterface $urlBuilder
     */
    private \Magento\Framework\UrlInterface $urlBuilder;

    /**
     * @var Authorization $authorization
     */
    private \Elogic\Internship\Model\Authorization $authorization;

    /**
     * @param \Elogic\Internship\Model\Authorization $authorization
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Elogic\Internship\Model\Authorization $authorization,
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->authorization = $authorization;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Remove column is editing and/or deleting stores is not allowed
     *
     * @inheridoc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function prepare(): void
    {
        parent::prepare();

        $editAllowed = $this->authorization->isAllowed(Authorization::ACTION_STORE_LOCATOR_EDIT);

        if (!$editAllowed) {
            $config = $this->getConfiguration();
            $config['componentDisabled'] = true;
            $this->setData('config', $config);
        }
    }

    /**
     * @inheritDoc
     */
    public function prepareDataSource(array $dataSource): array
    {
        $editAllowed = $this->authorization->isAllowed(Authorization::ACTION_STORE_LOCATOR_EDIT);
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item['store_entity_id']) && $editAllowed) {
                    $item[$this->getData('name')]['edit'] = [
                        'href' => $this->urlBuilder->getUrl(
                            static::URL_PATH_EDIT,
                            [
                                'store_entity_id' => $item['store_entity_id'],
                            ]
                        ),
                        'label' => __('Edit')
                    ];
                }
            }
        }
        return $dataSource;
    }
}
