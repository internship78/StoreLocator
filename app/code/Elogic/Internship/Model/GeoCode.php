<?php

declare(strict_types=1);

namespace Elogic\Internship\Model;

use Elogic\Internship\Api\GeoCodeInterface;
use Elogic\Internship\Model\ConfigProvider;

class GeoCode implements GeoCodeInterface
{

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    public function __construct(
        ConfigProvider $configProvider
    ) {
        $this->configProvider = $configProvider;
    }

    /**
     * @param string $address
     * @return array|string
     * @throws \JsonException
     */
    public function getCoordinates(string $address)
    {
        $apiKey = $this->configProvider->getGoogleMapsApiKey();
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key='.$apiKey);

        if (strpos($geo, 'error_message')) {
            $coordinates = 'ErrorApi';
        } elseif (strpos($geo, 'ZERO_RESULTS')) {
            $coordinates = 'ZERO_RESULTS';
        } else {
            $geo = json_decode($geo, true, 512, JSON_THROW_ON_ERROR);
            if (isset($geo['status']) && ($geo['status'] === 'OK')) {
                $coordinates[0] = $geo['results'][0]['geometry']['location']['lat'];
                $coordinates[1] = $geo['results'][0]['geometry']['location']['lng'];
            }
        }
        return $coordinates;
    }
}
