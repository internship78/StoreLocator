<?php

declare(strict_types=1);

namespace Elogic\Internship\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;


class StoreLocator extends AbstractDb
{

    public const ENTITY_TABLE_NAME = 'store_locator';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::ENTITY_TABLE_NAME, 'store_entity_id');
    }
}
