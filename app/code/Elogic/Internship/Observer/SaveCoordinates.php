<?php

declare(strict_types=1);

namespace Elogic\Internship\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Elogic\Internship\Model\GeoCode;

class SaveCoordinates implements ObserverInterface
{

    /**
     * @var GeoCode
     */
    private $geoCode;

    public function __construct(GeoCode $geoCode)
    {
        $this->geoCode = $geoCode;
    }


    /**
     * @param Observer $observer
     * @return array|mixed|void|null
     */
    public function execute(Observer $observer)
    {
        $store = $observer->getData('store');
        $data = $store->getData();

            if(!empty($data['longitude'])&& !empty($data['latitude'])) {
                return $store;
            }
            else {
                $address = $store->getAddress();
                $coordinates = $this->geoCode->getCoordinates($address);
                $store->setLatitude((string)$coordinates[1]);
                $store->setLongitude((string)$coordinates[0]);
                return $store;
            }
    }
}
